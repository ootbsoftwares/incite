<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master_batches extends Model
{
    protected $fillable = [
        'id', 'name', 'createdBy', 'modifiedBy', 'created_at', 'updated_at', 'is_active'
    ];

    public function users()
    {
        return $this->belongsTo('App\User', 'modifiedBy', 'id');
    }
}
