<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{
    protected $fillable = [
        'id', 'course_id', 'batch_id', 'video_url', 'createdBy', 'modifiedBy', 'created_at', 'updated_at'
    ];

    public function batches()
    {
        return $this->belongsTo('App\Master_batches', 'batch_id', 'id');
    }

    public function courses()
    {
        return $this->belongsTo('App\Master_courses', 'course_id', 'id');
    }
}
