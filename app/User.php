<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'registration_number', 'mobile', 'email_verified_at', 'dob', 'gender', 'address', 'user_type', 'password', 'createdBy', 'modifiedBy', 'remember_token', 'created_at', 'updated_at', 'is_active', 'course_id', 'batch_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function batches()
    {
        return $this->belongsTo('App\Master_batches', 'batch_id', 'id');
    }

    public function courses()
    {
        return $this->belongsTo('App\Master_courses', 'course_id', 'id');
    }
}
