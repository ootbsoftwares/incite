<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Videos;
use App\User;
use App\Master_courses;
use App\Master_batches;
use Response;
use Auth;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function students()
    {
        $students = User::with('courses', 'batches')->get()->toArray();
        return view('students', compact('students'));
    }

    public function add_student()
    {
        $courses = Master_courses::get()->pluck('name','id')->toArray();
        $batches = Master_batches::get()->pluck('name','id')->toArray();
        return view('add_student', compact('courses', 'batches'));
    }

    public function add_video()
    {
        $courses = Master_courses::get()->pluck('name','id')->toArray();
        $batches = Master_batches::get()->pluck('name','id')->toArray();
        return view('add_video', compact('courses', 'batches'));
    }

    public function save_student(Request $request)
    {
        try
        {
            $students_post_data = $request->all();
            $students_post_data['password'] = bcrypt($request->password);
            $students_post_data['createdBy'] = 1;
            $students_post_data['modifiedBy'] = 1;
            $students_post_data['is_active'] = 1;
            $students_post_data['user_type'] = 'Student';
            unset($students_post_data['_token']);
            User::create($students_post_data);
            return redirect('students');
            // return Response::json(array('success' => true, 'msg' => 'Video added successfully.'));
        }
        catch (\Throwable $th) 
        {
            return Response::json(array('success' => false, 'msg' => $th->getMessage() . ' on line ' . $th->getLine() . ' in file ' . $th->getFile()));
        }
    }

    public function save_video(Request $request)
    {
        try
        {
            $videos_post_data = $request->all();
            $videos_post_data['createdBy'] = 1;
            $videos_post_data['modifiedBy'] = 1;
            unset($videos_post_data['_token']);
            Videos::create($videos_post_data);
            return redirect('videos');
            // return Response::json(array('success' => true, 'msg' => 'Video added successfully.'));
        }
        catch (\Throwable $th) 
        {
            return Response::json(array('success' => false, 'msg' => $th->getMessage() . ' on line ' . $th->getLine() . ' in file ' . $th->getFile()));
        }
        

    }

    public function get_videos(Request $request)
    {
        try
        {
            if(Auth::user()->user_type == 'Admin')
            {
                $videos = Videos::with('courses', 'batches')->get()->toArray();
            }
            else
            {
                $videos = Videos::with('courses', 'batches')->where('course_id', Auth::user()->course_id)->where('batch_id', Auth::user()->batch_id)->get()->toArray();
            }
            
            return view('video_classes', compact('videos'));
        }
        catch (\Throwable $th) 
        {
            return Response::json(array('success' => false, 'msg' => $th->getMessage() . ' on line ' . $th->getLine() . ' in file ' . $th->getFile()));
        }
        

    }
}
