<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Master_courses;
use App\Master_batches;

class MasterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_courses(Request $request)
    {
        $courses = Master_courses::with('users')->get()->toArray();
        return view('courses', compact('courses'));
    }

    public function add_course()
    {
        return view('add_course');
    }

    public function save_course(Request $request)
    {
        try
        {
            $courses_post_data = $request->all();
            $courses_post_data['createdBy'] = 1;
            $courses_post_data['modifiedBy'] = 1;
            unset($courses_post_data['_token']);
            Master_courses::create($courses_post_data);
            return redirect('courses');
        }
        catch (\Throwable $th) 
        {
            return Response::json(array('success' => false, 'msg' => $th->getMessage() . ' on line ' . $th->getLine() . ' in file ' . $th->getFile()));
        }
    }

    public function get_batches(Request $request)
    {
        $batches = Master_batches::with('users')->get()->toArray();
        return view('batches', compact('batches'));
    }

    public function add_batch()
    {
        $courses = Master_courses::get()->pluck('name','id')->toArray();
        return view('add_batch', compact('courses'));
    }

    public function save_batch(Request $request)
    {
        try
        {
            $batches_post_data = $request->all();
            $batches_post_data['course_id'] = NULL;
            $batches_post_data['createdBy'] = 1;
            $batches_post_data['modifiedBy'] = 1;
            unset($batches_post_data['_token']);
            Master_batches::create($batches_post_data);
            return redirect('batches');
        }
        catch (\Throwable $th) 
        {
            return Response::json(array('success' => false, 'msg' => $th->getMessage() . ' on line ' . $th->getLine() . ' in file ' . $th->getFile()));
        }
    }
}
