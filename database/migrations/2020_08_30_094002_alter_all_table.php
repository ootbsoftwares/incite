<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_batches', function (Blueprint $table) {
            $table->foreign('createdBy')->references('id')->on('users');
			$table->foreign('modifiedBy')->references('id')->on('users');
        });

        Schema::table('master_courses', function (Blueprint $table) {
            $table->foreign('createdBy')->references('id')->on('users');
			$table->foreign('modifiedBy')->references('id')->on('users');
        });

        Schema::table('videos', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('master_courses');
            $table->foreign('batch_id')->references('id')->on('master_batches');
            $table->foreign('createdBy')->references('id')->on('users');
			$table->foreign('modifiedBy')->references('id')->on('users');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('master_courses');
            $table->foreign('batch_id')->references('id')->on('master_batches');
            $table->foreign('createdBy')->references('id')->on('users');
			$table->foreign('modifiedBy')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_batches', function (Blueprint $table) {
            //
        });
    }
}
