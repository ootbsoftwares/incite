<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id')->unsigned()->index();
            $table->integer('course_id')->unsigned()->nullable();
            $table->integer('batch_id')->unsigned()->nullable();
            $table->string('video_url')->nullable();
            $table->tinyInteger('is_active')->nullable();
            $table->integer('createdBy')->unsigned()->nullable();
            $table->integer('modifiedBy')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
