<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unsigned()->index();
            $table->string('name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('registration_number')->nullable();
            $table->string('mobile')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->date('dob')->nullable();
            $table->enum('gender', ['Male', 'Female', 'Other'])->nullable();
            $table->text('address')->nullable();
            $table->string('user_type')->nullable();
            $table->string('password')->nullable();
            $table->tinyInteger('is_active')->nullable();
            $table->integer('course_id')->unsigned()->nullable();
            $table->integer('batch_id')->unsigned()->nullable();
            $table->integer('createdBy')->unsigned()->nullable();
            $table->integer('modifiedBy')->unsigned()->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
