( function ( $ ) {
    "use strict";


    var ctx = document.getElementById( "barChart" );
    var myChart = new Chart( ctx, {
        type: 'bar',
        data: {
            labels: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul" ],
            datasets: [
                {
                    label: "Zoom Live",
                    data: [ 35, 46, 60, 45, 66, 49, 57 ],
                    borderColor: "rgba(0, 0, 0, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(0, 0, 0, 0.6)"
                            },
							{
                    label: "YouTube",
                    data: [ 65, 59, 80, 91, 56, 55, 45 ],
                    borderColor: "rgba(4, 103, 187, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(4, 103, 187, 0.5)"
                            },
                {
                    label: "Pre-Recorded",
                    data: [ 28, 48, 40, 19, 86, 27, 76 ],
                    borderColor: "rgba(0,0,0,0.09)",
                    borderWidth: "0",
                    backgroundColor: "rgba(0,0,0,0.07)"
                            }
                        ]
        },
        options: {
            scales: {
                yAxes: [ {
                    ticks: {
                        beginAtZero: true
                    }
                                } ]
            }
        }
    } );

    
    


   



    var ctx = document.getElementById( "pieChart" );
    ctx.height = 300;
    var myChart = new Chart( ctx, {
        type: 'pie',
        data: {
            datasets: [ {
                data: [ 45, 25, 20 ],
                backgroundColor: [
                                    "rgba(4, 103, 187,0.9)",
                                    "rgba(4, 103, 187,0.7)",
                                    "rgba(4, 103, 187,0.5)"
                                ],
                hoverBackgroundColor: [
                                   "rgba(4, 103, 187,0.9)",
                                    "rgba(4, 103, 187,0.7)",
                                    "rgba(4, 103, 187,0.5)"
                                ]

                            } ],
            labels: [
                            "Zoom Live",
							"YouTube",
                            "Pre-Recorded"
                        ]
        },
        options: {
            responsive: true
        }
    } );

  



} )( jQuery );