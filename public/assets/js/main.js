$.noConflict();

jQuery(document).ready(function($) {

	"use strict";

	[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
		new SelectFx(el);
	});

	jQuery('.selectpicker').selectpicker;


	

	$('.search-trigger').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').addClass('open');
	});

	$('.search-close').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		$('.search-trigger').parent('.header-left').removeClass('open');
	});

	$('.equal-height').matchHeight({
		property: 'max-height'
	});

	$('.count').each(function () {
		$(this).prop('Counter',0).animate({
			Counter: $(this).text()
		}, {
			duration: 3000,
			easing: 'swing',
			step: function (now) {
				$(this).text(Math.ceil(now));
			}
		});
	});

	$('#menuToggle').on('click', function(event) {
		var windowWidth = $(window).width();   		 
		if (windowWidth<1010) { 
			$('body').removeClass('open'); 
			if (windowWidth<760){ 
				$('#left-panel').slideToggle(); 
			} else {
				$('#left-panel').toggleClass('open-menu');  
			} 
		} else {
			$('body').toggleClass('open');
			$('#left-panel').removeClass('open-menu');  
		} 
			 
	}); 

	 
	$(".menu-item-has-children.dropdown").each(function() {
		$(this).on('click', function() {
			var $temp_text = $(this).children('.dropdown-toggle').html();
			$(this).children('.sub-menu').prepend('<li class="subtitle">' + $temp_text + '</li>'); 
		});
	});

	$(window).on("load resize", function(event) { 
		var windowWidth = $(window).width();  		 
		if (windowWidth<1010) {
			$('body').addClass('small-device'); 
		} else {
			$('body').removeClass('small-device');  
		} 
		
	});
	
	$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
});

$(function() {
    var $checkbox = '<input type="checkbox" checked="checked "data-toggle="toggle" data-onstyle="danger" class="on-off-btn" data-size="mini" />'
    $('.toggle-placeholder').html($checkbox);
    $('.toggle-placeholder').find('input[type=checkbox][data-toggle=toggle]')
                            .each(function(){
        $(this).bootstrapToggle();
    });
});
 $(function() {
    $('.status').change(function() {
        alert("Are you sure, you want to disable the student!");
    })
  })
 
  
 
});

jQuery(document).ready(function () {

            jQuery("#addButton").click(function () {
                if( (jQuery('.form-horizontal .control-group').length+1) > 5) {
                    alert("Only 5 options allowed");
                    return false;
                }
                var id = (jQuery('.form-horizontal .control-group').length + 1).toString();
                jQuery('.form-horizontal').append('<div class="control-group" id="control-group' + id + '"><label class="control-label" for="inputEmail' + id + '">Option ' + id + '</label><div class="controls' + id + '"><textarea type="text" class="form-control" rows="3"></textarea></div></div>');
            });

           jQuery("#removeButton").click(function () {
                if ($('.form-horizontal .control-group').length == 1) {
                    alert("No more textbox to remove");
                    return false;
                }

               jQuery(".form-horizontal .control-group:last").remove();
            });
        });

