<footer class="site-footer">
   <div class="footer-inner bg-white">
      <div class="row">
         <div class="col-sm-6">
            © <script type="text/javascript">
                  document.write(new Date().getFullYear());
               </script> Incite Academy. All Rights Reserved 
         </div>
         <div class="col-sm-6 text-right">
            Designed by <a href="http://www.cosycreatives.com">Cosy Creatives</a>
         </div>
      </div>
   </div>
</footer>