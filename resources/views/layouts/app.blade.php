<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Incite Academy</title>
      <meta name="description" content="Concorde Academy">
      
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('assets/css/font-icons.min.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('assets/css/style.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('assets/css/chartist.min.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('assets/css/jquery-ui.css') }}">
      <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap-datepicker.css') }}">
      @yield('extra_css')
      <style>
            @media print {
         html, body {
            display: none;  /* hide whole page */
         }
      }
      </style>
      
   </head>
   <body>
      <aside id="left-panel" class="left-panel">
         <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
               <ul class="nav navbar-nav">
                  <li class="active">
                     <a href="{{ url('/') }}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                  </li>
                  @if(Auth::user()->user_type == 'Admin')
                  <li>
                     <a href="{{ url('/courses') }}"><i class="menu-icon fa fa-list"></i>Courses </a>
                  </li>
                  <li>
                     <a href="{{ url('/batches') }}"><i class="menu-icon fa fa-list"></i>Batches </a>
                  </li>
                  <li>
                     <a href="{{ url('/videos') }}"><i class="menu-icon fa fa-list"></i>Videos </a>
                  </li>
                  <li>
                     <a href="{{ url('/students') }}"><i class="menu-icon fa fa-users"></i>Students </a>
                  </li>
                  @endif
                  <li>
                     <a href="#"> <i class="menu-icon fa fa-bullseye"></i>Zoom Live Classes</a>                   
                  </li>
                  <li>
                     <a href="{{ url('/videos') }}"><i class="menu-icon fa fa-youtube"></i>YouTube Live Classes</a>
                  </li>
                  <li>
                     <a href="#"><i class="menu-icon fa fa-video-camera"></i>Pre-Recorded Videos</a>
                  </li>                
                  <li>
                     <a href="#"><i class="menu-icon fa fa-cog"></i>Settings</a>
                  </li>
               </ul>
            </div>
         </nav>
      </aside>
      <div id="right-panel" class="right-panel">
         <header id="header" class="header">
            <div class="top-left">
               <div class="navbar-header"> 
                  <a class="navbar-brand" href="{{ url('/') }}"><img src="assets/images/logo.png" alt="Logo"></a>
                  <a class="navbar-brand hidden" href="{{ url('/') }}"><img src="assets/images/logo.png" alt="Logo"></a> 
                  <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a> 
               </div>
            </div>
            <div class="top-right">
               <div class="header-menu">
                  
                  <div class="user-area dropdown float-right">
                     <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <img class="user-avatar rounded-circle" src="assets/images/admin.jpg" alt="Admin">
                     </a>
                     <div class="user-menu dropdown-menu">
                        <a class="nav-link" href="#"><i class="fa fa-cog"></i>Settings</a>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </header>
         <div>
            @yield('content')
         </div>
         <footer class="site-footer">
            <div class="footer-inner bg-white">
               <div class="row">
                  <div class="col-sm-6">
                     © <script type="text/javascript">
                           document.write(new Date().getFullYear());
                        </script> Incite Academy. All Rights Reserved 
                  </div>
                  <div class="col-sm-6 text-right">
                     Designed by <a href="http://www.cosycreatives.com">Cosy Creatives</a>
                  </div>
               </div>
            </div>
         </footer>
      </div>
      <script src="{{ URL::asset('assets/js/jquery.min.js') }}" type="text/javascript"></script>
      <script defer src="{{ URL::asset('assets/js/bootstrap-datepicker.js') }}"></script>
      <script src="{{ URL::asset('assets/js/popper.min.js') }}" type="text/javascript"></script>
      <script src="{{ URL::asset('assets/js/plugins.js') }}" type="text/javascript"></script>
      <script src="{{ URL::asset('assets/js/main.js') }}" type="text/javascript"></script>
      <script src="{{ URL::asset('assets/js/Chart.bundle.min.js') }}"></script>
      <script src="{{ URL::asset('assets/js/chartjs-init.js') }}"></script>
      @yield('extra_js')
      <script>
        jQuery.ajaxSetup({
            headers: {
               'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
         });
    </script>
   </body>
</html>