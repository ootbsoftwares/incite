@extends('layouts.app')
@section('content')        
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Batches</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Batches</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="content">
        
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Batch List</strong>
                            <div class="pull-right messages-buttons">
                                <a href="{{ url('/add-batch') }}" class="btn  btn-success button">Add Batch</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Name</th>
                                        <th>Action By</th>
                                        <th>Actioned At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1; @endphp
                                    @foreach($batches as $key => $value)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{{ $value['name'] }}</td>
                                            <td>{{ $value['users']['name'] }}</td>
                                            <td>{{ date('d-m-Y h:i:s', strtotime($value['updated_at'])) }}</td>
                                            <td>
                                                <a href="#"> <button type="button" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></button></a>
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                        @php $i++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection