<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Incite Academy</title>
    <meta name="description" content="Concorde Academy">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-icons.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

</head>
<body>


<section class="fxt-template-animation fxt-template-layout1">
        <div class="container-fluid">
            <div class="row">
            <div class="col-md-6 col-12 fxt-none-767 fxt-bg-img"  style="background-image:url(assets/images/bg1-l.jpg)"></div>
                <div class="col-md-6 col-12 fxt-bg-color">
                    <div class="fxt-content">
                        <div class="fxt-header">
                            <a href="{{ route('login') }}" class="fxt-logo"><img src="assets/images/logo.png" alt="Logo"></a>
                            <div class="fxt-page-switcher">
                                <a href="{{ route('login') }}" class="switcher-text1">Log In</a>
                                <a href="{{ route('register') }}" class="switcher-text1 active">Register</a>
                            </div>
                        </div>
                        <div class="fxt-form">
                            <h2>Register</h2>     
                            <p>Create an account now!</p>
                            <form method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}
                                <div class="form-group{{$errors->has('username') ? ' has-error' : '' }}"> 
                                    <input type="email" class="form-control" name="username" placeholder="Username"  value="{{ old('username') }}" required autofocus>
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{$errors->has('email') ? ' has-error' : '' }}">
                                    <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{$errors->has('phone_no') ? ' has-error' : '' }}">
                                    <input type="number" class="form-control" name="phone_no" placeholder="Phone No." required>
                                    @if ($errors->has('phone_no'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone_no') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input type="password" class="form-control" name="password" placeholder="Password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif

                                </div>
                               
                                <!-- <div class="form-group">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                                </div> -->
                                <div class="form-group">
                                        <div class="fxt-content-between">
                                            <button type="submit" class="fxt-btn-fill">Register</button>
                                    </div>
                                </div>
                            </form>                            
                        </div> 
                        
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <script src="assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="assets/js/popper.min.js" type="text/javascript"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }); 
    </script>
</body>

</html>

