@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
      <div class="row m-0">
          <div class="col-sm-4">
              <div class="page-header float-left">
                  <div class="page-title">
                      <h1>Student</h1>
                  </div>
              </div>
          </div>
          <div class="col-sm-8">
              <div class="page-header float-right">
                  <div class="page-title">
                      <ol class="breadcrumb text-right">
                          <li><a href="#"><i class="fa fa-home"></i></a></li>
                          <li><a href="{{ url('/students') }}">Students</a></li>
                          <li class="active">Add Student</li>
                          
                      </ol>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="content">
	<div class="row">
        <div class="col-md-8 offset-md-2">
	        <div class="card">
	            <div class="card-header"><strong>Add Student</strong>
	                <div class="pull-right messages-buttons">
	                	<a href="{{ url('/students') }}" class="btn  btn-primary button">List</a></div>
					</div>
					<form method="POST" action="{{ url('/save-student') }}" autocomplete="off">
					<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
						<div class="card-body card-block">
							<div class="form-group">
								<label for="RegistrationNumber" class=" form-control-label">Registration Number <span class="text-red">*</span></label>
								<input type="text" id="name" placeholder="Registration Number" name="registration_number" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="Name" class=" form-control-label">Name<span class="text-red">*</span></label>
								<input type="text" id="name" placeholder="Enter student name" name="name" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="Username" class=" form-control-label">Username <span class="text-red">*</span></label>
								<input type="text" id="username" name="username" placeholder="Enter student Username" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="Password" class="form-control-label">Password </label>
								<input type="password" id="password" name="password" placeholder="Enter Password" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="email" class="form-control-label">Email <span class="text-red">*</span></label>
								<input type="email" id="email" name="email" placeholder="Enter Email" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="mobile" class="form-control-label">Mobile <span class="text-red">*</span></label>
								<input type="text" id="mobile" name="mobile" placeholder="Enter mobile" class="form-control" required>
							</div>
							<div class="form-group">
								<label for="dob" class="form-control-label">Date of Birth <span class="text-red">*</span></label>
								<input type="text" id="dob" name="dob" placeholder="Enter Date of Birth" class="form-control datepicker" autocomplete="off" required>
							</div>
							<div class="form-group">
								<label for="Gender" class="form-control-label">Gender <span class="text-red">*</span></label>
								<select class="form-control" id="gender" name="gender" required>
									<option value="">-- Select --</option>
									<option value="Male">Male</option>
									<option value="Female">Female</option>
									<option value="Other">Other</option>
								</select>
							</div>
							<div class="form-group">
								<label for="phone" class="form-control-label">Address <span class="text-red">*</span></label>
								<textarea name="address" id="textarea-input" rows="2" placeholder="Address..." class="form-control" required></textarea>
							</div>
							
							<div class="form-group">
								<label for="Course" class="form-control-label">Course <span class="text-red">*</span></label>
								<select class="form-control" id="course_id" name="course_id" required>
									<option value="">-- Select --</option>
									@foreach($courses as $key => $value)
										<option value="{{ $key }}">{{ $value }} </option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="Batch" class="form-control-label">Batch <span class="text-red">*</span></label>
								<select class="form-control" id="batch_id" name="batch_id" required>
									<option value="">-- Select --</option>
									@foreach($batches as $key => $value)
										<option value="{{ $key }}">{{ $value }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-actions form-group">
								<input class="btn btn-success pull-right" type="submit" value="Submit">
							</div>
						</div>
					</form>
	            </div>
	        </div>
	    </div>
	</div>

	
@endsection
@section('extra_js')
<script>
		jQuery(document).ready(function() 
         {
			jQuery('.datepicker').datepicker({
				format: 'yyyy/mm/dd',
			});
		 });
    </script>
@endsection