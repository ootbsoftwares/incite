@extends('layouts.app')

@section('content')
<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Student</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#"><i class="fa fa-home"></i></a></li>
                            <li><a href="#">Students</a></li>
                            <li class="active">Bulk Student Upload</li>
                            
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

        
<div class="content">
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-header"><strong>Bulk Student Upload</strong>
                    <div class="pull-right messages-buttons"><a href="students.html" class="btn  btn-primary button">List</a>
                    </div>
                </div>
                <div class="card-body card-block text-center">
                    <a href="exl/student_template.xlsx" class="btn btn-info">Download Template</a>
                   	<hr>
                    <div class="form-group">
                        <label for="file-input" class=" form-control-label">Upload Excel</label>
                        <input type="file" id="file-input" name="file-input" class="form-control-file form-control">
                    </div>
                    <hr>  
                    <div class="form-actions form-group">
                        <input class="btn btn-success pull-right" type="submit" value="Upload">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection