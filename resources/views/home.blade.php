@extends('layouts.app')

@section('content')
    <div class="content pb-0">
       <div class="row">
          <div class="col-lg-3 col-md-6">
             <div class="card">
                <div class="card-body">
                   <div class="stat-widget-five">
                      <div class="stat-icon dib flat-color-6">
                         <i class="pe-7f-users"></i>
                      </div>
                      <div class="stat-content">
                         <div class="text-left dib">
                            <div class="stat-text"><span class="count">1875</span></div>
                            <div class="stat-heading">Enrolled Students</div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
          <div class="col-lg-3 col-md-6">
             <div class="card">
                <div class="card-body">
                   <div class="stat-widget-five">
                      <div class="stat-icon dib flat-color-5">
                         <i class="pe-7f-albums"></i>
                      </div>
                      <div class="stat-content">
                         <div class="text-left dib">
                            <div class="stat-text"><span class="count">02</span></div>
                            <div class="stat-heading">Courses</div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
          <div class="col-lg-3 col-md-6">
             <div class="card">
                <div class="card-body">
                   <div class="stat-widget-five">
                      <div class="stat-icon dib flat-color-3">
                         <i class="pe-7f-video"></i>
                      </div>
                      <div class="stat-content">
                         <div class="text-left dib">
                            <div class="stat-text"><span class="count">152</span></div>
                            <div class="stat-heading">Live Classes</div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
          <div class="col-lg-3 col-md-6">
             <div class="card">
                <div class="card-body">
                   <div class="stat-widget-five">
                      <div class="stat-icon dib flat-color-4">
                         <i class="pe-7f-browser"></i>
                      </div>
                      <div class="stat-content">
                         <div class="text-left dib">
                            <div class="stat-text"><span class="count">253</span></div>
                            <div class="stat-heading">Pre-Rec Classes</div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <div class="clearfix"></div>
       <div class="orders">
          <div class="row">
             <div class="col-xl-8">
                <div class="card">
                   <div class="card-body">
                      <h4 class="box-title">New Students </h4>
                   </div>
                   <div class="card-body--">
                      <div class="table-stats order-table ov-h">
                         <table class="table ">
                            <thead>
                               <tr>
                                  <th class="serial">Reg. No.</th>
                                  <th class="avatar">Image</th>
                                  <th>Name</th>
                                  <th>Course</th>
                                  <th>Email</th>
                               </tr>
                            </thead>
                            <tbody>
                               <tr>
                                  <td class="serial">5469</td>
                                  <td class="avatar">
                                     <div class="round-img">
                                        <a href="#"><img class="rounded-circle" src="images/1.jpg" alt=""></a>
                                     </div>
                                  </td>
                                  <td><span class="name">Manikandan</span> </td>
                                  <td> <span class="product">NEET</span> </td>
                                  <td>manikandan@gmail.com</td>
                               </tr>
                               <tr>
                                  <td class="serial">5468</td>
                                  <td class="avatar">
                                     <div class="round-img">
                                        <a href="#"><img class="rounded-circle" src="images/2.jpg" alt=""></a> 
                                     </div>
                                  </td>
                                  <td>  <span class="name">Gregory John</span> </td>
                                  <td> <span class="product">CA</span> </td>
                                  <td>gregory@gmail.com</td>
                               </tr>
                               <tr>
                                  <td class="serial">5467</td>
                                  <td class="avatar">
                                     <div class="round-img">
                                        <a href="#"><img class="rounded-circle" src="images/3.jpg" alt=""></a> 
                                     </div>
                                  </td>
                                  <td>  <span class="name">Deepak Kumar</span> </td>
                                  <td> <span class="product">NEET</span> </td>
                                  <td>deepak@gmail.com</td>
                               </tr>
                               <tr>
                                  <td class="serial">5466</td>
                                  <td class="avatar">
                                     <div class="round-img">
                                        <a href="#"><img class="rounded-circle" src="images/4.jpg" alt=""></a> 
                                     </div>
                                  </td>
                                  <td>  <span class="name">Reena </span> </td>
                                  <td> <span class="product">NEET</span> </td>
                                  <td>reena@gmail.com</td>
                               </tr>
                               <tr class=" pb-0">
                                  <td class="serial">5465</td>
                                  <td class="avatar pb-0">
                                     <div class="round-img">
                                        <a href="#"><img class="rounded-circle" src="images/6.jpg" alt=""></a> 
                                     </div>
                                  </td>
                                  <td>  <span class="name">Rajesh Mathan</span> </td>
                                  <td> <span class="product">CA</span> </td>
                                  <td>rajesh@gmail.com</td>
                               </tr>
                            </tbody>
                         </table>
                      </div>
                   </div>
                </div>
             </div>
             <div class="col-xl-4">
                <div class="row">
                   <div class="col-lg-6 col-xl-12">
                      <div class="card br-0">
                         <div class="card-body">
                            <h4 class="mb-3">Student Chart </h4><br>

                            <canvas id="pieChart"></canvas>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <div class="row">
          <div class="col-lg-12">
             <div class="card">
                <div class="card-body">
                   <h4 class="card-title box-title">Student Count</h4>
                   <div class="card-content">
                      <canvas id="barChart"></canvas>
                   </div>
                </div>
             </div>
          </div>
          
       </div>
    </div>
    <div class="clearfix"></div>
@endsection
