@extends('layouts.app')
@section('extra_css')
<style>
    .holder{
    width: 400px;
    height: 300px;
    position: relative;
}

.frame{
    width: 100%;
    height: 100%;
}

.bar{
    position: fixed;
    top: 22 !important;
    left: 0;
    width: 100%;
    height: 40px;
    font-weight: 600;
}
</style>
@endsection
@section('content')
         
<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Videos</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#"><i class="fa fa-home"></i></a></li>
                            <li class="active">Videos</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
    
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        @if(Auth::user()->user_type == 'Admin')
                            <div class="pull-right messages-buttons">
                                <a href="{{ url('/add-video') }}" class="btn  btn-success button">Add Video</a>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                            @foreach ($videos as $key => $value) 
                                <div class="col-md-3">
                                <img src="https://img.youtube.com/vi/<?php echo substr($value['video_url'], strrpos($value['video_url'], '/') + 1) ?>/hqdefault.jpg" class="click_to_open_frame" video-url="<?php echo $value['video_url'] ?>" style="height: 140px; width: 210px" id="open_frame"/>
                                    <hr>
                                    <span><b>Course:</b> {{ $value['courses']['name'] }} <br> <b>Batch:</b> {{ $value['batches']['name'] }} </span>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 100%" style="pointer-events: none;">
    <div class="modal-dialog" style="width: 100%">
        <div class="modal-content pull-left" style="width: 100%">
        
            <div class="holder" style="width: 100%" id="yt-player">
            <div class="bar" style="color: white;">@php echo 'User Name: '.Auth::user()->name.' | Mobile: '.Auth::user()->mobile @endphp</div>
                <iframe src="" frameborder="0" allow="accelerometer;  encrypted-media; gyroscope; picture-in-picture" style="width: 245%; height: 589px; margin-left: -72%;"></iframe>
                
            </div>
        </div>
    </div>
</div>
@endsection
@section('extra_js')

<script>
    jQuery(document).on("contextmenu",function(){
       return false;
    }); 
    jQuery(document).keydown(function (event) {
        if (event.keyCode == 123) { // Prevent F12
            return false;
        } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
            return false;
        }
    });

    jQuery('.click_to_open_frame').click(function () {
        jQuery('#myModal').modal('show');
        jQuery('#myModal iframe').attr('src', jQuery(this).attr('video-url')+"?controls=0&showinfo=0&enablejsapi=1");
    });

    jQuery('#myModal button').click(function () {
        jQuery('#myModal iframe').removeAttr('src');
        
    });

    jQuery('#myModal').on('hidden.bs.modal', function () {
        jQuery('#myModal iframe').removeAttr('src');
    });
</script>
@endsection