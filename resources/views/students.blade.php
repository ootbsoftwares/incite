@extends('layouts.app')
@section('content')        
    <div class="breadcrumbs">
        <div class="breadcrumbs-inner">
            <div class="row m-0">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Students</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Students</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="content">
        
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Student List</strong>
                            <div class="pull-right messages-buttons">
                         
                        <a href="#" class="btn  btn-primary button">Bulk Upload</a>
                        <a href="{{ url('/add-student') }}" class="btn  btn-success button">Add Student</a>
                         
                    </div>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Reg. No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Course</th>
                                        <th>Batch</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i = 1; @endphp
                                @foreach($students as $key => $value)
                                    @if($value['user_type'] != 'Admin')
                                    <tr>
                                       <td>{{ $i }}</td>
                                        <td>{{ $value['name'] }}</td>
                                        <td>{{ $value['email'] }}</td>
                                        <td>{{ $value['mobile'] }}</td>
                                        <td>{{ $value['courses']['name'] }}</td>
                                        <td>{{ $value['batches']['name'] }}</td>
                                        <td>
                                            <a href="#"> <button type="button" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></button></a>
                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                    @php $i++; @endphp
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection
@section('extra_js')
<script>
    var  courses = <?php $courses; ?>
    var  batches = <?php $batches; ?>
</script>
@endsection