@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
      <div class="row m-0">
          <div class="col-sm-4">
              <div class="page-header float-left">
                  <div class="page-title">
                      <h1>Course</h1>
                  </div>
              </div>
          </div>
          <div class="col-sm-8">
              <div class="page-header float-right">
                  <div class="page-title">
                      <ol class="breadcrumb text-right">
                          <li><a href="#"><i class="fa fa-home"></i></a></li>
                          <li><a href="#">Courses</a></li>
                          <li class="active">Add Course</li>
                          
                      </ol>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="content">
	<div class="row">
        <div class="col-md-8 offset-md-2">
	        <div class="card">
	            <div class="card-header"><strong>Add Course</strong>
	                <div class="pull-right messages-buttons">
	                	<a href="{{ url('/courses') }}" class="btn  btn-primary button">List</a></div>
					</div>
					<form method="POST" action="{{ url('/save-course') }}" autocomplete="off">
					<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
						<div class="card-body card-block">
							<div class="form-group">
								<label for="URL" class=" form-control-label">Course Name <span class="text-red">*</span></label>
								<input type="text" id="name" name="name" placeholder="Enter Course Name" class="form-control" required>
							</div>
							<div class="form-actions form-group">
								<input class="btn btn-success pull-right" type="submit" value="Submit">
							</div>
						</div>
					</form>
	                
	            </div>
	        </div>
	    </div>
	</div>

	
@endsection
@section('extra_js')
<script>
		jQuery(document).ready(function() 
         {
			jQuery('.datepicker').datepicker({
				format: 'dd/mm/yyyy',
			});
		 });
    </script>
@endsection