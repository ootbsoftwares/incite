@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
  <div class="breadcrumbs-inner">
      <div class="row m-0">
          <div class="col-sm-4">
              <div class="page-header float-left">
                  <div class="page-title">
                      <h1>Batche</h1>
                  </div>
              </div>
          </div>
          <div class="col-sm-8">
              <div class="page-header float-right">
                  <div class="page-title">
                      <ol class="breadcrumb text-right">
                          <li><a href="#"><i class="fa fa-home"></i></a></li>
                          <li><a href="#">Batches</a></li>
                          <li class="active">Add Batch</li>
                          
                      </ol>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="content">
	<div class="row">
        <div class="col-md-8 offset-md-2">
	        <div class="card">
	            <div class="card-header"><strong>Add Batch</strong>
	                <div class="pull-right messages-buttons">
	                	<a href="{{ url('/batches') }}" class="btn  btn-primary button">List</a></div>
					</div>
					<form method="POST" action="{{ url('/save-batch') }}" autocomplete="off">
					<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
						<div class="card-body card-block">
							<div class="form-group">
								<label for="URL" class=" form-control-label">Batch Name <span class="text-red">*</span></label>
								<input type="text" id="name" name="name" placeholder="Enter Batch Name" class="form-control" required>
							</div>
							<!-- <div class="form-group">
								<label for="Course" class="form-control-label">Course<span class="text-red">*</span></label>
								<select class="form-control" id="course_id" name="course_id">
									<option>-- Select --</option>
									@foreach($courses as $key => $value)
										<option value="{{ $key }}">{{ $value }}</option>
									@endforeach
								</select>
							</div> -->
							<div class="form-actions form-group">
								<input class="btn btn-success pull-right" type="submit" value="Submit">
							</div>
						</div>
					</form>
	                
	            </div>
	        </div>
	    </div>
	</div>

	
@endsection
@section('extra_js')
<script>
		jQuery(document).ready(function() 
         {
			jQuery('.datepicker').datepicker({
				format: 'dd/mm/yyyy',
			});
		 });
    </script>
@endsection