<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'StudentController@index');

Route::get('/students', 'StudentController@students');
Route::get('/add-student', 'StudentController@add_student');
Route::post('/save-student', 'StudentController@save_student');
Route::get('/add-video', 'StudentController@add_video');
Route::post('/save-video', 'StudentController@save_video');
Route::get('/videos', 'StudentController@get_videos');
Route::get('/add-course', 'MasterController@add_course');
Route::post('/save-course', 'MasterController@save_course');
Route::get('/courses', 'MasterController@get_courses');
Route::get('/add-batch', 'MasterController@add_batch');
Route::post('/save-batch', 'MasterController@save_batch');
Route::get('/batches', 'MasterController@get_batches');
